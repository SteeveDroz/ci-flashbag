<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ComposerAdapter
{
    private $object;

    public function __construct($object)
    {
        $this->object = $object;
    }

    public function __call($method, $args)
    {
        return call_user_func_array([$this->object, $method], $args);
    }
}
