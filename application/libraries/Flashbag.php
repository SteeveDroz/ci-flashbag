<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require('ComposerAdapter.php');

class Flashbag extends ComposerAdapter
{
    public function __construct()
    {
        parent::__construct(new SteeveDroz\CiFlashbag\Flashbag());
    }
}
