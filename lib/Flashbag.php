<?php
defined('BASEPATH') OR exit('No direct script access allowed');

namespace SteeveDroz\CiFlashbag;

class Flashbag {
    private $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->library('session');
    }

    public function add($message, $type = 'info')
    {
        $this->CI->session->set_flashdata(uniqid(), new Flash($message, $type));
    }

    public function show()
    {
        $messages = [];
        $flashdata = $this->CI->session->flashdata();
        foreach ($flashdata as $flash)
        {
            $messages[] = $flash->show();
        }
        return implode(PHP_EOL, $messages);
    }
}
