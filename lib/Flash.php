<?php

namespace SteeveDroz\CiFlashbag;

class Flash
{
    const ALLOWED_TYPES = ['debug', 'info', 'warning', 'error', 'fatal'];

    private $message;
    private $type;

    public function __construct($message, $type)
    {
        if (!in_array($type, self::ALLOWED_TYPES))
        {
            throw new Exception('Cannot find type. It must be one the values: ' . implode(', ', self::ALLOWED_TYPES));
        }

        $this->message = $message;
        $this->type = $type;
    }

    public function show()
    {
        return '<div class="flashbag ' . $this->type . '">' . $this->message . '</div>';
    }
}
